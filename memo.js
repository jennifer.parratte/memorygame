class game {
    constructor() {

        this.divSet = [
            {
                'name': 'div1',
                'nbr' : '🐙',
            },
            {
                'name': 'div2',
                'nbr' : '🦉',
            },
            {
                'name': 'div3',
                'nbr' : '🐏',
            },
            {
                'name': 'div4',
                'nbr' : '🐒',
            },
            {
                'name': 'div5',
                'nbr' : '🦔',
            },
            {
                'name': 'div6',
                'nbr' : '🐿️',
            },
            {
                'name': 'div7',
                'nbr' : '🐯',
            },
            
        ];
        this.deckGame = document.querySelector('#game');
        this.grid = document.createElement('section');
        this.grid.setAttribute('class', 'grid');
        this.deckGame.appendChild(this.grid);

        //Initialisation de count à 0
        this.count = 0;

        //variables pour le compteur
        this.moves = 0;
        this.counter = document.querySelector('.nbMoves');
        this.counter.innerHTML = "Nombre de coups 0"

        //variables pour le timer
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.timer = document.querySelector('.timer');
        this.timer.innerHTML = "Temps écoulé 0:0"
        this.interval;
        

        //variables pour les choix
        this.firstChoice = '';
        this.secondChoice = '';
        this.previousTarget = null;


        //déclaration de la variable por récupérer toutes les cartes qui match
        this.matchedCards = document.getElementsByClassName('match');

        //concaténation du tableau de cartes à lui même pour avoir le jeu complet
        this.TotalDivSet = this.divSet.concat(this.divSet);
        //randomisation du jeu (https://forum.freecodecamp.org/t/how-does-math-random-work-to-sort-an-array/151540)
        this.TotalDivSet.sort(() => 0.5 - Math.random());

        this.TotalDivSet.forEach(item => {
            let card = document.createElement('div');
            card.classList.add('card');
            
            card.dataset.type = item.name;
           
            //création de la div .front
            let front = document.createElement('div');
            front.classList.add('front');

            //création de la div .back
            let back = document.createElement('div');
            back.classList.add('back');
            

            let nbr = document.createElement('a');
            nbr.setAttribute('class', 'fas');
            nbr.innerHTML = item.nbr;

            this.grid.appendChild(card);
            card.appendChild(front);
            card.appendChild(back);
            back.appendChild(nbr);
            
        });
    }




    resetChoices() {
        //reset des variables
        this.count = 0;
        this.firstChoice = '';
        this.secondChoice = '';
         //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Suppression de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.remove('selected', 'unmatch');
        })
    }

    //Méthode affichant la victoire
    congratulation() {
        //récupération du temps total
        let finalTime = this.timer.innerHTML;    
        alert(this.moves+' coups et '+finalTime);
    }


    match(){
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'match' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('match');
        })
          //Gestion de la victoire
          if (this.matchedCards.length == 14){
            this.congratulation();
        }

    }

    unmatch(){
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('unmatch');
        })
    }

    //Méthode pour rejouer
    playAgain() {
        window.location.reload();
    }

    //Méthode du compteur de coups
    startCounter() {
        this.moves++;
        this.counter.innerHTML = " Nombre de coups " + this.moves;

        if(this.moves == 1){
            this.startTimer();
        }
    }

    //Méthode du timer
    startTimer() {
        //interval toutes les secondes
        this.interval = setInterval(() =>{
            //Affichage
            this.timer.innerHTML= "Temps écoulé "+ this.minute + " : " + this.second;
            this.second++;
            //Incrémente minute toutes les 60s
            if(this.second == 60){
                this.minute++;
                this.second = 0;
            }
            //Incrémente hour toutes les 60m
            if(this.minute == 60){
                this.hour++;
                this.minute = 0;
            }

        }, 1000);
    }

            //fonction qui lance le jeu
    start(){
        this.grid.addEventListener('click', event => {
            //récupération de l'élement cliqué
            let clicked = event.target;

            //return vide si on clique sur la section
            if (clicked.nodeName === 'SECTION' ||
            clicked === this.previousTarget ||
            clicked.parentNode.classList.contains('selected')) {
                return;

                
            }
            //limiter le nombre de coups par 2
            if (this.count < 2) {
                //itérateur de count
                this.count++;
                console.log("ppl3 : "+event.target);  
                if (this.count === 1){
                    //récupération du data-name
                    this.firstChoice = clicked.parentNode.dataset.type;
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                    //start le compteur
                    this.startCounter();

                } else {
                    //deuxième essai
                    //récupération du data-name
                    this.secondChoice = clicked.parentNode.dataset.type;
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                }

                //Si nos choix sont pas vides
                if (this.firstChoice !== '' && this.secondChoice !== ''){
                    //Si les 2 choix sont identiques
                    if (this.firstChoice === this.secondChoice){
                        //Run la fonction match
                        this.match();
                        //Run la fonction resetChoices
                        this.resetChoices();
                    } else {
                        //Run la fonction unmatch
                        this.unmatch();
                        setTimeout(this.resetChoices.bind(this), 900);
                    }
                }
                //set du previousTarget
                this.previousTarget = clicked;
            }
        })
    }
}

let letsPlay = new game;
//run de la fonction start()
letsPlay.start();